#!/bin/sh

# 1. upscale to 4x4 the size
/home/user/Downloads/ffmpeg-4.3.2-amd64-static/ffmpeg -i CIMG3454.AVI -vf "scale=iw*4:ih*4" -sws_flags neighbor -vcodec libx264 -preset ultrafast -crf 0 -an tmp1.mkv

# 2. add black margins above and under the video
/home/user/Downloads/ffmpeg-4.3.2-amd64-static/ffmpeg -i tmp1.mkv -vf "scale=896:512:force_original_aspect_ratio=decrease,pad=896:512:-1:-1:color=black" -sws_flags neighbor -vcodec libx264 -preset ultrafast -crf 0 -an tmp2.mkv

# 3. add text on margin with the current frame (which eqals to milliseconds at 1000fps)
/home/user/Downloads/ffmpeg-4.3.2-amd64-static/ffmpeg -i tmp2.mkv -vf "drawtext=fontfile=Arial.ttf: text='time\: %{frame_num}ms': start_number=1: x=(w-tw)/2: y=h-(2*lh): fontcolor=black: fontsize=20: box=1: boxcolor=white: boxborderw=4" -vcodec libx264 -preset ultrafast -crf 0 -an output.mkv

# 4. delete tmp files
rm ./tmp1.mkv
rm ./tmp2.mkv

# start video and enjoy (shortcut for single frame display can be configured in VLC!)
vlc output.mkv
